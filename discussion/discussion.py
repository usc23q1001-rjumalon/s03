for x in range (6,10,2):
	print(f"the current value is {x}")
	
test = int(input("Enter number: "))

if test % 3 == 0 and test % 5 == 0:
	print("Number is divisible by both 3 and 5")
elif test % 3 == 0:
	print("Number is divisible by 3")
elif test % 5 == 0:
	print("Number is divisible by 5")
else:
	print("Number is not divisible by 3 and 5")
